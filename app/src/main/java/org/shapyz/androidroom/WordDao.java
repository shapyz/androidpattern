package org.shapyz.androidroom;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

@Dao
interface WordDao {

    @Insert
    void insert(Word word);

    @Query( "SELECT * FROM word_table ORDER BY word ASC" )
    LiveData<List<Word>> getAllWords();

    @Query( "DELETE FROM word_table" )
    void DeleteAll();
}
