package org.shapyz.androidroom;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.os.AsyncTask;

import java.util.List;

public class WordRepository {
    private WordDao mWordDao;
    private LiveData<List<Word>> mAllWords;

    public WordRepository(Application application) {
        WordRoomDatabase database = WordRoomDatabase.getDatabase( application );
        this.mWordDao = database.wordDao();
        this.mAllWords = mWordDao.getAllWords();
    }

    public void insert(Word word) {
        new insertAsycTask(mWordDao).execute(word);
    }

    public LiveData<List<Word>> getAllWords() {
        return mAllWords;
    }

    private class insertAsycTask extends AsyncTask<Word, Void, Void> {
        private WordDao mAsyncTaskDao;
        public insertAsycTask(WordDao mWordDao) {
            mAsyncTaskDao = mWordDao;
        }

        @Override
        protected Void doInBackground(Word... words) {
            mAsyncTaskDao.insert( words[0] );
            return null;
        }
    }
}
